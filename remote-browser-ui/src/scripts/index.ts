import RemoteBrowser from "./RemoteBrowser.js";


let loadingImg: string = "http://joofthan.com/images/locked.png";
const image: HTMLImageElement =  document.getElementById("image") as HTMLImageElement;
let timeout:any;

//let browser = new RemoteBrowser("http://localhost:8080");
let browser = new RemoteBrowser("https://remote-browser-server.herokuapp.com");


function addListener(elementId: string, onclick: () => Promise<void>) {
    const element: HTMLElement = document.getElementById(elementId) as HTMLImageElement;
    element.onclick = onclick;
}

async function loadPlaceholder(){
    image.src = loadingImg;
}

async function fadeOut(){
    clearTimeout(timeout);
    if(image.src.length === 0){
        loadPlaceholder();
    }else{
        image.style.opacity = "0.2";
    }
}

async function loadImage(imageData: Promise<string>, silent: boolean = false) {
    if (image === null) throw new Error("image tag not found");
    if(!silent)fadeOut();
    //image.src = loadingImg;
    image.src = await imageData;
    setTimeout(()=>{image.style.opacity = "1";},200);
}

async function search(){
    let text = prompt("Suchtext eingeben") as string;
    if(text == null) throw new Error("Kein Text Eingegeben");
    await loadImage(browser.search(text));
}

async function url(){
    let text = prompt("Url eingeben", "http://joofthan.com") as string;
    if(text == null) throw new Error("Kein Text Eingegeben");
    fadeOut();
    await browser.loadPage(text);
    drawPageImage();
}

async function drawPageImage(){
    clearTimeout(timeout);
    await loadImage(browser.screenshot());
    timeout = setTimeout(async function(){ await loadImage(browser.screenshot(), true) }, 8000);
}


addListener("search", search);
addListener("url", url);
addListener("close", async () => {
    fadeOut();
    await browser.close();
    loadPlaceholder();
});


image.addEventListener('click', clickPosition, true);
async function clickPosition(e:any){

      let cursorX = e.pageX - image.offsetLeft;
      let cursorY= e.pageY - image.offsetTop;
      fadeOut();
      await browser.click(cursorX,cursorY);
      drawPageImage();
}
