export default class RemoteBrowser{
    private endpoint: string;

    constructor(endpoint:string) {
        this.endpoint = endpoint;
    }

    public async search(text: string):Promise<string>{
        let url = "https://www.google.de/search?q="+ encodeURIComponent(text);
        console.log(url);
        let result = await fetch(this.endpoint+"/page/"+url).then(r=>r.blob());
        let base64ImageData = await this.convertBlobToBase64(result);
        return base64ImageData as string;
    }

    public async loadPage(url: string){
        return await fetch(this.endpoint+"/page/"+url);
    }

    public async close() {
        await fetch(this.endpoint+"/close/");
    }

    public async click(cursorX: number, cursorY: number) {
        await fetch(this.endpoint+"/clickposition/"+cursorX+"/"+cursorY);
    }

    public async screenshot(){
        let result = await fetch(this.endpoint+"/screenshot/").then(r=>r.blob());
        let base64ImageData = await this.convertBlobToBase64(result);
        return base64ImageData as string;
    }

    private async convertBlobToBase64(blob:Blob):Promise<string | ArrayBuffer | null> {
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onerror = reject;
            reader.onload = () => {
                resolve(reader.result);
            };
            reader.readAsDataURL(blob);

        });

    }

}