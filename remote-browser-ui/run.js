import express from "express";

const app = express();
const port = 3000;

// Setting up the public directory
app.use(express.static('target'));

app.listen(port, () => console.log(`listening on port ${port}!`));