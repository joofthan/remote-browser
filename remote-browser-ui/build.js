import fs from "fs"

var source = './src/public/';
var dir = fs.readdirSync(source);
for (var i=0; i < dir.length; i++) {
    fs.createReadStream(source+dir[i]).pipe(fs.createWriteStream("target/"+dir[i]));
}
