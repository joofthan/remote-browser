package com.joofthan.remotebrowser.spring;

import com.joofthan.remotebrowser.core.Entity;
import org.springframework.stereotype.Service;
import org.springframework.web.context.annotation.ApplicationScope;

import javax.annotation.PreDestroy;

@Service
@ApplicationScope
public class BrowserManager extends com.joofthan.remotebrowser.selenium.BrowserManager {
    @PreDestroy
    public Entity close(){
        return super.close();
    }
}