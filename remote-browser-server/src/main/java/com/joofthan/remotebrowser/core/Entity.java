package com.joofthan.remotebrowser.core;

public class Entity {
    String name;
    int alter;

    public Entity(String name, int alter) {
        this.name = name;
        this.alter = alter;
    }

    public String getName() {
        return name;
    }

    public int getAlter() {
        return alter;
    }
}
