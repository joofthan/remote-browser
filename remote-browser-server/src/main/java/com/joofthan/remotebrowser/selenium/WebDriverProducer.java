package com.joofthan.remotebrowser.selenium;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;

import java.util.concurrent.TimeUnit;
import java.util.logging.LogManager;
import java.util.logging.Logger;


public class WebDriverProducer {
    public static WebDriver produceWebDriver(boolean headless) {
        WebDriver driver;
        try{
            driver = tryCreateChromeDriver(headless);
        }catch (Exception e){
            try{
                driver = tryCreateEdgeDriver();
            } catch (Exception e2){
                try{
                    driver = tryCreateFirefoxDriver(headless);
                } catch (Exception e3){
                    System.err.println(e.getMessage());
                    System.err.println(e2.getMessage());
                    System.err.println(e3.getMessage());
                    throw new RuntimeException(e.getMessage() + "-----------------\n"+e2.getMessage() +"-----------------\n"+e3.getMessage());
                }
            }
        }

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().setSize(new Dimension(1280,4000));

        return driver;
    }

    private static WebDriver tryCreateFirefoxDriver(boolean headless) {
        WebDriver driver;
        System.setProperty("webdriver.gecko.driver", "remote-browser-server/modules/webbrowserDriver/geckodriver");
        FirefoxBinary firefoxBinary = new FirefoxBinary();
        FirefoxOptions options = new FirefoxOptions();
        options.setBinary(firefoxBinary);
        options.setHeadless(headless);
        driver = new FirefoxDriver(options);
        return driver;
    }

    private static WebDriver tryCreateEdgeDriver() {
        WebDriver driver;
        try{
            System.setProperty("webdriver.edge.driver", "modules\\webbrowserDriver\\msedgedriver.exe");
            driver = new EdgeDriver();
        }catch (Exception e){
            try{
                System.setProperty("webdriver.edge.driver", "..\\webbrowserDriver\\msedgedriver.exe");
                driver = new EdgeDriver();
            }catch(Exception e2){
                System.setProperty("webdriver.edge.driver", "..\\modules\\webbrowserDriver\\msedgedriver.exe");
                driver = new EdgeDriver();
            }
        }
        return driver;
    }

    private static WebDriver tryCreateChromeDriver(boolean headless) {
        try{
            return new ChromeDriver(createChromeOptions(headless));
        } catch (Exception e) {
            WebDriver driver;
            System.setProperty("webdriver.chrome.driver", "chromedriver");
            ChromeOptions options = createChromeOptions(headless);
            options.setHeadless(headless);
            options.setBinary(System.getenv("GOOGLE_CHROME_BIN"));
            driver = new ChromeDriver(options);
            return driver;
        }
    }

    private static ChromeOptions createChromeOptions(boolean headless) {
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(headless);
        options.addArguments("disable-dev-shm-usage");
        return options;
    }
}