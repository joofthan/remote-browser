package com.joofthan.remotebrowser.selenium;


import com.joofthan.remotebrowser.core.Entity;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.springframework.web.context.annotation.ApplicationScope;

import java.io.File;

import static java.util.Objects.requireNonNull;

@ApplicationScope
public class BrowserManager {

    private WebDriver driver;

    private WebDriver getDriver() {
        if (driver == null) {
            driver = WebDriverProducer.produceWebDriver(true);
        }
        return driver;
    }

    public Entity page(String url) {
        getDriver().get(url);
        return new Entity(url, 10);
    }

    public Entity click(String cssSelector) {
        getDriver().findElement(By.cssSelector(cssSelector)).click();
        return new Entity(cssSelector, 10);
    }

    public Entity close() {
        if(driver != null) driver.close();
        driver = null;
        return new Entity("closed", 10);
    }

    public Entity click(Integer x, Integer y) {
        requireNonNull(x);
        requireNonNull(y);
        Actions act = new Actions(getDriver());
        act.moveByOffset(x, y).click().moveByOffset(-x, -y).build().perform();
        return new Entity(x+" "+y, 10);
    }

    public File screenshot() {
        File file = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
        return file;
    }

    public void reset() {
        driver = null;
    }
}
