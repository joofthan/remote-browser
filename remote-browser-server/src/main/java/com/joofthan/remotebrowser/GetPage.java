package com.joofthan.remotebrowser;

import com.joofthan.remotebrowser.core.Entity;
import com.joofthan.remotebrowser.spring.BrowserManager;
import io.swagger.v3.oas.annotations.Hidden;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.openqa.selenium.WebDriverException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@CrossOrigin
@RestController("/")
public class GetPage {

    @Autowired
    private BrowserManager browserManager;

    @Hidden
    @GetMapping("/")
    void home(HttpServletResponse response) throws IOException {
        response.sendRedirect("/swagger-ui.html");
    }

    @GetMapping(path = "page/**" , produces = MediaType.IMAGE_PNG_VALUE)
    void page(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String url = getFullURL(request).split("/page/")[1];
        System.out.println(url);
        try{
            browserManager.page(url);
        }catch(WebDriverException e){
            browserManager.reset();
            browserManager.page(url);
        }
        screenshot(response);
    }

    public static String getFullURL(HttpServletRequest request) {
        StringBuilder requestURL = new StringBuilder(request.getRequestURL().toString());
        String queryString = request.getQueryString();

        if (queryString == null) {
            return requestURL.toString();
        } else {
            return requestURL.append('?').append(queryString).toString();
        }
    }

    @GetMapping(path = "click/{cssSelector}" , produces = MediaType.APPLICATION_JSON_VALUE)
    Entity click(@PathVariable String cssSelector){
        return browserManager.click(cssSelector);
    }

    @GetMapping(path = "clickposition/{x}/{y}")
    void clickposition(HttpServletResponse response, @PathVariable Integer x, @PathVariable Integer y) throws IOException, InterruptedException {
        browserManager.click(x, y);
        screenshot(response);
    }

    @GetMapping(path = "screenshot", produces = MediaType.IMAGE_PNG_VALUE)
    void screenshot(HttpServletResponse response) throws IOException {
        InputStream in = new FileInputStream(browserManager.screenshot());
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }

    @GetMapping(path = "close" , produces = MediaType.APPLICATION_JSON_VALUE)
    Entity close(){
        return browserManager.close();
    }


}
