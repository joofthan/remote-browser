package com.joofthan.remotebrowser;

import com.joofthan.remotebrowser.spring.BrowserManager;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MainTests {

	@Autowired
    BrowserManager browserManager;

	@Test
	void clickGames() {
		browserManager.page("http://joofthan.com");
		browserManager.click(388,30);
		browserManager.close();
	}

}
